import React from "react"
import RichText from "../richText"
import styled from "styled-components"
import StyledLinkButton from '../common/StyledLinkButton'
import { hexToRgbA, darkenLighten } from '../common/ColorAdjust'

const HeroWrapper = styled.section`
  --banner-bg-color: ${props => props.transparentColors.banner_bg_color};
  --text-and-border-color-button: red;
  --text-and-border-color-hover-button: black;

  margin-left: -15px;
  margin-right: -15px;
  height: calc(100vh - 54px);
  background: ${props => props.bgcolor ? props.bgcolor : "var(--header-bg-color)"};

  /* The div is the box containing the banner text in mobile views full screen */
  div{
    padding: 0 2rem !important;
    text-align: center;
    // center vertically
    vertical-align: middle;
    width: 100%;
    display: inline-block;
    height: 50%;
    position: absolute;
    top: 0;
    bottom: 0%;
    margin: auto;
      }

    div p{
      color: red !important;
      font-size: 1.6rem;
      margin-top: 2.6rem;
      margin-bottom: 2.4rem;
      }

    div h1{
      color: green !important;
    }

  // Small devices (landscape phones, 576px and up)
  @media (min-width: 576px) {
    margin-top: ${props => props.navbarstyle ? '-63px' : 0};
    color: white;
    background: url(${props => props.bgimagephonelandscape});
    background-size: cover;
    background-repeat: no-repeat;
    display: flex;
    align-items: center;
    text-align: center;
    height: 100vh;

      div{
        background: var(--banner-bg-color);
        // center within the bg minus the header
        top: 63px;
      }

      div h1{
        margin: -0.5rem 0;
        font-size: 2rem;
      }

      div p{
        margin: 1.6rem 0;
        font-size: 1.5rem;
      }
}

  // Medium devices (tablets, 768px and up)
  @media (min-width: 768px) {
    background: url(${props => props.bgimagetablet});
    margin-top: ${props => props.navbarstyle ? '-72px' : 0};

/*     margin-top: 0;
    height: calc(100vh - 70px);
    background: ${props => props.bgcolor ? props.bgcolor : "var(--header-bg-color)"};
    
    div{
      padding: 0 2rem !important;
      text-align: center;
      // center this div vertically
      vertical-align: middle;
      width: 100%;
      display: inline-block;
      height: 50%;
      position: absolute;
      top: 0;
      bottom: 0%;
      margin: auto;
      }
    } */
  }

  // Large devices (desktops, 992px and up)
  @media (min-width: 992px) {
    background: url(${props => props.bgimagelaptop});
    background-size: cover;
    /* drags the hero behind the transparent navbar, does not work in ipadlandscape(1024x768) */
    margin-top: ${props => props.navbarstyle ? '-90px' : 0};

      div{
        max-width: 600px;
        background-color: black;
    }

      div h1{
        padding: 1rem;
        margin: 0 0;
        font-size: 2rem;
      }

      div p{
        margin: 1.6rem 0;
        font-size: 1.5rem;
      }
}

  // Extra large devices (large desktops, 1200px and up)
  @media (min-width: 1200px) {
    background: url(${props => props.bgimage});
    background-size: cover;
    
      div{
        max-width: 800px; 
  }
}
`;

const StyledButton = styled(StyledLinkButton)`
  background: ${props => props.button_bg_color ? props.button_bg_color : null};
  color: ${props => props.button_color ? props.button_color : null} !important;
  border-color: ${props => props.button_color ? props.button_color : null};
  font-size: 1.5rem;
  padding: 0.8rem 1.6rem;

      &:hover{
        color: ${props => props.button_color ? darkenLighten(props.button_color, 60) : null} !important;
        border-color: ${props => props.button_color ? darkenLighten(props.button_color, 60) : null};
        background: ${props => props.button_bg_color ? darkenLighten(props.button_bg_color, -60) : null};
    }
`;

const Hero = ({ 
  title, 
  content, 
  ctabuttontarget, 
  ctabuttonlabel,
  transparentnavbar, 
  bgimage, 
  bgimagetablet, 
  bgimagelaptop, 
  bgimagephone, 
  bgimagephonelandscape, 
  bgcolor,
  banner_bg_transparency,
  button_bg_color,
  button_color,
  button_transparent,
  banner_bg_color
}) => {
  if (button_transparent === true) {
    button_bg_color = "transparent"
  }

  let transparentColors = "";

  if (bgcolor) {
    transparentColors = {
      banner_bg_color: hexToRgbA(bgcolor, (banner_bg_transparency / 100))
    };
  }

  return (
    <HeroWrapper
      navbarstyle={transparentnavbar}
      bgimage={bgimage}
      bgimagetablet={bgimagetablet}
      bgimagelaptop={bgimagelaptop}
      bgimagephone={bgimagephone}
      bgimagephonelandscape={bgimagephonelandscape}
      bgcolor={bgcolor}
      transparentColors={transparentColors}
      banner_bg_color={banner_bg_color}
    >
      <div>
        <RichText render={title} />
        <p>
          {content}
        </p>
        <StyledButton
          to={`${ctabuttontarget}`}
          button_bg_color={button_bg_color}
          button_color={button_color}
        >{ctabuttonlabel}</StyledButton>
      </div>
    </HeroWrapper>

  )
}

export default Hero;

