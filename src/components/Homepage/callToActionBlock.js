import React from 'react'
import RichText from "../richText"
import StyledLinkButton from "../common/StyledLinkButton"
import StyledAnchorLinkButton from '../common/StyledAnchorLinkButton'
import { Card } from "react-bootstrap"
import styled from 'styled-components'

const StyledCard = styled(Card)`
    // determins breakepoint
    min-width: 24rem;
    margin-bottom: 1.5rem !important;
    background: var(--component-bg-color);
`;

export default function ({ title, content, buttonLabel, buttonDestination, featuredImage, anchorLink, anchorLinkLabel }) { //destructure from the props
    const anchorLinkComponent = (
        <StyledAnchorLinkButton to={`/#${anchorLink}`} className="btn btn-outline-danger">
            {anchorLinkLabel}
        </StyledAnchorLinkButton>
    );

    const goToPageLink = (
        <StyledLinkButton to={buttonDestination} className="btn btn-outline-danger">
            {buttonLabel}
        </StyledLinkButton>
    );
    
    return (
        <StyledCard>
            <Card.Body>
                {featuredImage ? // check if image was delivered and render it conditionally
                    <Card.Img src={featuredImage} alt="featured" className="featured-image-wrapper" />
                    : null}
                <Card.Title>
                    <RichText render={title} />
                </Card.Title>
                <RichText render={content} />
                {anchorLink ? anchorLinkComponent : goToPageLink}
            </Card.Body>
        </StyledCard>
    )
}
