import React from "react"
import Hero from "./hero"
import CallToActionGrid from "./callToActionGrid"
import TeaserSection from './TeaserSection'
import PageTitle from '../../components/PageTitle'
import OneColTextSection from '../../components/OneColTextSection'
import CardGallery from './Cardgallery'
import Carousel from './Carousel'

const SliceZone = ({ node }) => {
  console.log('SliceZone Node', node);
  return (
    <div>
      {node.site_title ? <PageTitle pageTitle={node.site_title} /> : null}
      {node.content ? <OneColTextSection content={node.content} /> : null}
      
      {node.body.map((bodyContent, i) => {
        if (bodyContent.type === "hero") {
          console.log('HeroContent: ', bodyContent);
          return (
            <Hero key={i}
              transparentnavbar={bodyContent.primary.menue_transparent}
              title={bodyContent.primary.hero_title}
              content={bodyContent.primary.hero_content}
              bgimage={bodyContent.primary.background_image ? bodyContent.primary.background_image.url : null}
              bgimagetablet={bodyContent.primary.background_image.tablet ? bodyContent.primary.background_image.tablet.url : null}
              bgimagelaptop={bodyContent.primary.background_image.laptop ? bodyContent.primary.background_image.laptop.url : null}
              bgimagephone={bodyContent.primary.background_image.phone ? bodyContent.primary.background_image.phone.url : null}
              bgimagephonelandscape={bodyContent.primary.background_image.phone_landscape ? bodyContent.primary.background_image.phone_landscape.url : null}
              ctabuttonlabel={bodyContent.primary.hereo_cta_button_label}
              ctabuttontarget={bodyContent.primary.cta_target ? bodyContent.primary.cta_target._meta.uid : null}
              bgcolor={bodyContent.primary.bg_color}
              text_color={bodyContent.primary.text_color}
              banner_bg_transparency={bodyContent.primary.banner_bg_transparency}
              button_bg_color={bodyContent.primary.button_bg_color}
              button_color={bodyContent.primary.button_color}
              button_transparent={bodyContent.primary.button_transparent}
              banner_bg_color={bodyContent.primary.banner_bg_color}
            />
          )
        }
        if (bodyContent.type === "call_to_action_grid") {
          return (
            <CallToActionGrid
              key={i}
              title={bodyContent.primary.section_title}
              callToActions={bodyContent.fields}
            />
          )
        } if (bodyContent.type === "anrisstext") {
          return (
            <TeaserSection
              key={i}
              teasers={bodyContent.fields}
            />
          )
        }
        if (bodyContent.type === "carousel") {
          return (
            <CardGallery
              key={i}
              images={bodyContent}
              title={bodyContent.primary.section_title}
            />
          )
        } 
        if (bodyContent.type === "bilder-karussell") {
          return (
            <Carousel
              key={i}
              images={bodyContent}
            />
          )
        }
        else { return null; }
      })}
    </div>
  )
};

export default SliceZone;