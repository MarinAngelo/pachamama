import styled from 'styled-components'
import Modal from 'react-bootstrap/Modal'

const StyledModal = styled(Modal)`
    // Extra small devices (portrait phones, less than 576px)
// No media query for xs since this is the default in Bootstrap

// Small devices (landscape phones, 576px and up, col-sm)
@media (min-width: 576px) {

}
// Medium devices (tablets, 768px and up, col-md)
@media (min-width: 768px) {

}
// Large devices (desktops, 992px and up, col-lg)
@media (min-width: 992px) {
    .modal-xl {
    max-width: 1400px;
}
    .modal-header{
        border-bottom: 0;
        padding: 0;
        margin-top: -1rem;
    }
    .modal-content{
        background-color: #002700;

    }
    .close{
        color: white;
    }
}
// Extra large devices (large desktops, 1200px and up, col-xl)
@media (min-width: 1200px) {

}
`

export default StyledModal