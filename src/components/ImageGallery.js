import React, { useState } from 'react'
import Card from 'react-bootstrap/Card'
import CardDeck from 'react-bootstrap/CardDeck'
import Carousel from '../components/Homepage/Carousel'
import StyledModal from '../components/common/StyledModal'
import Modal from 'react-bootstrap/Modal'

function ImageGallery({ images }) {

    // Modal
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    // End Modal

    console.log('image in imageGallery', images);
    if (images.primary.gallerie_type === "Link nach Bildkarussell"){
        console.log('Gallery type', images.primary.gallerie_type);
        return (
            <>
            <CardDeck>
                    {images.fields.map((image, i) => (
                    <Card key={i}>
                        <Card.Link href="#" onClick={handleShow}>
                            <Card.Img src={image.image.url} />
                            <Card.ImgOverlay>
                            </Card.ImgOverlay>
                        </Card.Link>
                    </Card>
                ))}
            </CardDeck>
                <StyledModal
                    show={show}
                    onHide={handleClose}
                    backdrop="static"
                    keyboard={false}
                    size="xl"
                    centered
                    aria-labelledby="example-custom-modal-styling-title"
                >
                    <Modal.Body>
                        <Modal.Header closeButton />
                        <Carousel images={images} />
                    </Modal.Body>
                </StyledModal>
            </>
        )
    } if (images.primary.gallerie_type === "Link auf externe Site") {
        return (
            <CardDeck>
                {images.fields ? images.fields.map((image, i) => (
                    <Card key={i}>
                        <Card.Link href={image.link ? image.link.url : null} target={image.link ? image.link.target : null}>
                            <Card.Img src={image.image.url} />
                            <Card.ImgOverlay>
                            </Card.ImgOverlay>
                        </Card.Link>
                    </Card>
                )) : null}
            </CardDeck>
        )
    }
}

export default ImageGallery
