import React from 'react'
import ImageGallery from '../components/ImageGallery'
import TextInCols from '../components/TextInCols'


function PageSliceZone({node}) {
    return (
        <>
            {node ? node.body.map((bodyContent, i) => {
                if (bodyContent.type === "bildergalerie") {
                    return (
            <ImageGallery images={bodyContent} key={i} />
                    )
                }
                if (bodyContent.type === "textspalten") {
                    return (
            <TextInCols contents={bodyContent} key={i} />
                    )
                } else { return null; }
            }
            )
        : null}
        </>
    )
}

export default PageSliceZone
